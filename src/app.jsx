const App = () => {
    return (
        <div className="max-w-[1040px] mx-auto px-5">
            <h1 className="text-center my-10 font-bold text-xl">
                Hello everyon, it&apos;s reactjs simple template (tutorial) website.
            </h1>
            <div className="pb-20">
                <h3 className="font-bold text-lg">What's you need for use this template:</h3>
                <div className="pl-5">
                    <h3>
                        1. Visiul studio code{" "}
                        <a className="text-teal-300" rel="noopener noreferrer" target="_blank" href="https://code.visualstudio.com/">https://code.visualstudio.com/</a>.
                    </h3>
                    <h3>2. Install 'Live server' <a className="text-teal-300" href="https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer" target="_blank" rel="noopener noreferrer">https://marketplace.visualstudio.com/</a> plugin for visual studio code.</h3>
                    <h3>3. Install 'Git' <a className="text-teal-300" href="https://git-scm.com/" target="_blank" rel="noopener noreferrer">https://git-scm.com/</a> for your PC.</h3>
                </div>
            </div>
            <div className="items-center">
                <h2 className="font-bold text-lg">The author&apos;s contact:</h2>
                <a href="https://gitlab.com/Chachayev" target="_blank" >
                    <button className="font-black flex items-center">
                        Gitlab:
                        <img className="max-w-[40px] max-h-[40px]" src="../img/gitlab.svg" alt="" />
                        <span className="px-4 py-2 duration-300 rounded-lg hover:bg-teal-300 w-full transition-all">@Chachayev</span>
                    </button>
                </a>
            </div>
        </div>
    )
}

ReactDOM.render(<App />, document.querySelector("#root"))